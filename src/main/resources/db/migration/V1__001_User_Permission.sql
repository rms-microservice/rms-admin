
CREATE TABLE c_permission (
  id bigserial,
  name varchar(60) NOT NULL unique,
  created_on timestamp without time zone NOT NULL DEFAULT now(),
  updated_on timestamp without time zone NOT NULL DEFAULT now(),
  version bigint NOT NULL DEFAULT 0,
  PRIMARY KEY (id)
);
INSERT INTO c_permission VALUES
(1,'can_delete_user','1970-01-01 00:00:00','1970-01-01 00:00:00',0),
(2,'can_create_user','1970-01-01 00:00:00','1970-01-01 00:00:00',0),
(3,'can_update_user','1970-01-01 00:00:00','1970-01-01 00:00:00',0),
(4,'can_read_user','1970-01-01 00:00:00','1970-01-01 00:00:00',0);

CREATE TABLE c_role (
  id bigserial,
  name varchar(60) NOT NULL unique,
  created_on timestamp without time zone NOT NULL DEFAULT now(),
  updated_on timestamp without time zone NOT NULL DEFAULT now(),
  version bigint NOT NULL DEFAULT 0,
  PRIMARY KEY (id)
);
INSERT INTO c_role VALUES
(1,'role_admin','1970-01-01 00:00:00','1970-01-01 00:00:00',0),
(2,'role_user','1970-01-01 00:00:00','1970-01-01 00:00:00',0);

CREATE TABLE c_permission_role (
  permission_id bigint NOT NULL,
  role_id bigint NOT NULL,
  created_on timestamp without time zone NOT NULL DEFAULT now(),
  updated_on timestamp without time zone NOT NULL DEFAULT now(),
  version bigint NOT NULL DEFAULT 0,
  PRIMARY KEY (permission_id,role_id),
  CONSTRAINT c_permission_role_fk1 FOREIGN KEY (permission_id) REFERENCES c_permission (id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT c_permission_role_fk2 FOREIGN KEY (role_id) REFERENCES c_role (id) ON DELETE CASCADE ON UPDATE CASCADE
);

INSERT INTO c_permission_role VALUES
(1,1,'1970-01-01 00:00:00','1970-01-01 00:00:00',0),
(2,1,'1970-01-01 00:00:00','1970-01-01 00:00:00',0),
(3,1,'1970-01-01 00:00:00','1970-01-01 00:00:00',0),
(4,1,'1970-01-01 00:00:00','1970-01-01 00:00:00',0),
(4,2,'1970-01-01 00:00:00','1970-01-01 00:00:00',0);

CREATE TABLE c_user (
  id bigserial,
  username varchar(24) NOT NULL unique,
  password varchar(200) NOT NULL,
  email varchar(255) NOT NULL,
  enabled boolean NOT NULL,
  account_expired boolean NOT NULL,
  credentials_expired boolean NOT NULL,
  account_locked boolean NOT NULL,
  created_on timestamp without time zone NOT NULL DEFAULT now(),
  updated_on timestamp without time zone NOT NULL DEFAULT now(),
  version bigint NOT NULL DEFAULT 0,
  PRIMARY KEY (id)
);
INSERT INTO c_user VALUES
(1,'admin','{bcrypt}$2a$10$EOs8VROb14e7ZnydvXECA.4LoIhPOoFHKvVF/iBZ/ker17Eocz4Vi','admin@example.com', true,false,false,false,'1970-01-01 00:00:00','1970-01-01 00:00:00',0),
(2,'user','{bcrypt}$2a$10$EOs8VROb14e7ZnydvXECA.4LoIhPOoFHKvVF/iBZ/ker17Eocz4Vi','user@example.com',true,false,false,false,'1970-01-01 00:00:00','1970-01-01 00:00:00',0);

CREATE TABLE c_role_user (
  role_id bigint NOT NULL,
  user_id bigint NOT NULL,
  created_on timestamp without time zone NOT NULL DEFAULT now(),
  updated_on timestamp without time zone NOT NULL DEFAULT now(),
  version bigint NOT NULL DEFAULT 0,
  PRIMARY KEY (role_id,user_id),
  CONSTRAINT c_role_user_fk1 FOREIGN KEY (role_id) REFERENCES c_role (id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT c_role_user_fk2 FOREIGN KEY (user_id) REFERENCES c_user (id) ON DELETE CASCADE ON UPDATE CASCADE
);

INSERT INTO c_role_user VALUES
(1,1,'1970-01-01 00:00:00','1970-01-01 00:00:00',0),
(2,2,'1970-01-01 00:00:00','1970-01-01 00:00:00',0);
