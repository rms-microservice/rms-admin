package com.mitrais.rms.admin.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@Entity
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Table(name = "c_permission")
public class Permission extends BaseIdEntity {

	private static final long serialVersionUID = 1L;

	private String name;

}
