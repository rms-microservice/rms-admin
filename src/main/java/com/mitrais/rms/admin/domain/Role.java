package com.mitrais.rms.admin.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Table(name = "c_role")
public class Role extends BaseIdEntity {

	private static final long serialVersionUID = 1L;

	private String name;

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "c_permission_role",
			joinColumns = {@JoinColumn(name = "role_id", referencedColumnName = "id") },
			inverseJoinColumns = {@JoinColumn(name = "permission_id", referencedColumnName = "id") })
	private List<Permission> permissions;

	public Role(Long id, String name) {
		this.id = id; 
		this.name = name;
	}
}
