package com.mitrais.rms.admin.controller.dto;

import com.mitrais.rms.admin.domain.Role;
import com.mitrais.rms.admin.domain.User;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;


@Data
public class UserDTO {
    private Long id;
    private String username;
    private Boolean active;
    private List<Role> roles = new ArrayList<>();

    public UserDTO(){}

    public UserDTO(User u){
        this(u.getId(), u.getUsername(), u.isEnabled(), u.getRoles());
    }
    public UserDTO(Long id, String username, Boolean enabled, List<Role> roles){
        this.id = id;
        this.username = username;
        this.active = enabled;
        /*for(Role r: roles) {
        	roles.add(new Role(r.getId(), r.getName()));
        }*/
        
    }
}