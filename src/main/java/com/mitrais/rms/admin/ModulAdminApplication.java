package com.mitrais.rms.admin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class ModulAdminApplication {

	public static void main(String[] args) {
		SpringApplication.run(ModulAdminApplication.class, args);
	}

}
